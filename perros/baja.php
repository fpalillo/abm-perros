<?php

  include 'conexion.php';

  $id = $_POST['id'];

  $query = "DELETE FROM perro WHERE id = '$id'";

  $baja = mysqli_query($conexion, $query);

  if ($baja) {
    echo '
      <script>
        alert("El perro con id ', $id,' fue borrado.");
        window.location = "../baja.php";
      </script>
    ';
  } else {
    echo '
      <script>
        alert("No se pudo borrar el perro con id ', $id,'. Inténtelo nuevamente");
        window.location = "../baja.php";
      </script>
    ';
  }

  mysqli_close($conexion);

?>
