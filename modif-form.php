<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width-device-width, initial-scale=1.0">
  <title>ABM de perros - Modificación</title>
  <?php

    include 'perros/conexion.php';

    $id = $_POST['id'];

    $query = "SELECT * FROM perro WHERE id = '$id'";
    $modif = mysqli_query($conexion, $query);
    $perro = null;

    if (mysqli_num_rows($modif) > 0) {
      $perro = mysqli_fetch_assoc($modif);
    } else {
      echo '
      <script>
        alert("No existe el perro con id ', $id,'. Intente con otro id");
        window.location = "./modif.php";
      </script>
    ';
    }

    mysqli_close($conexion);

  ?>
</head>
<body>
  <main>
    <h2>Actualizar la información del perro con ID <?php echo $id; ?></h2>
    <form action="perros/modif.php" method="POST" class="">
      <input type="hidden" name="id" value="<?php echo $id; ?>">
      <label for="nombre">Nombre</label><br>
      <input type="text" maxlength="20" placeholder="Juan" name="nombre" value="<?php echo $perro['nombre']; ?>" required><br>

      <label for="raza">Raza</label><br>
      <input type="text" maxlength="25" placeholder="Doberman" name="raza" value="<?php echo $perro['raza']; ?>"><br>

      <label for="edad">Edad</label><br>
      <input type="number" min="0" max="20" placeholder="5" name="edad" value="<?php echo $perro['edad']; ?>" required><br>

      <label for="tamano">Tamaño</label><br>
      <input type="number" min="0" max="100" placeholder="Tamaño (en cm.)" name="tamano" value="<?php echo $perro['tamano']; ?>"><br>

      Sexo:<br>
      <input type="radio" name="sexo" value="M" <?php echo ($perro['sexo'] == True) ? "checked" : "" ; ?>>
      <label for="M">Macho</label><br>
      <input type="radio" name="sexo" value="H" <?php echo ($perro['sexo'] == False) ? "checked" : "" ; ?>>
      <label for="H">Hembra</label><br>

      <label for="macho">Ciudad</label><br>
      <input type="text" maxlength="25" placeholder="Ciudad" name="ciudad" value="<?php echo $perro['ciudad']; ?>" required><br>

      <label for="macho">Descripcion</label><br>
      <textarea maxlength="100" placeholder="Descripcion" name="descripcion" required><?php echo $perro['descripcion']; ?></textarea><br>

      <button type="submit">Modificar</button>
    </form>
  </main>
  <br><br>
  <a href="./index.php">< Volver</a>
</body>
</html>
